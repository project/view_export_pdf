<?php

namespace Drupal\view_export_pdf\WkhtmlToPdf;

use Symfony\Component\HttpFoundation\Response;
use mikehaertl\wkhtmlto\Pdf;

define('CORE_PDF_PDF_TOOL_DEFAULT', 'wkhtmltopdf');
define('CORE_PDF_PAPER_SIZE_DEFAULT', 'letter');
define('CORE_PDF_PAGE_ORIENTATION_DEFAULT', 'portrait');
define('CORE_PDF_WKHTMLTOPDF_OPTIONS', '--disallow-local-file-access  --footer-font-size 7');
define("CORE_PDF_DPI", "96");

//List of available commands for wkhtmltopdf
define('CMD_DISABLE_EXTERNAL_LINKS', '--disable-external-links');
define('CMD_DISABLE_INTERNAL_LINKS', '--disable-internal-links');
define('CMD_BOOK', '--book');
define('CMD_COLLATE', '--collate');
define('CMD_FORMS', '--enable-forms');
define('CMD_GRAYSCALE', '--grayscale');
define('CMD_LOWQUALITY', '--lowquality');
define('CMD_MARGIN_BOTTOM', '--margin-bottom');
define('CMD_MARGIN_LEFT', '--margin-left');
define('CMD_MARGIN_RIGHT', '--margin-right');
define('CMD_MARGIN_TOP', '--margin-top');
define('CMD_NO_BACKGROUND', '--no-background');
define('CMD_ORIENTATION', '--orientation');
define('CMD_PAGE_HEIGHT', '--page-height');
define('CMD_PAGE_SIZE', '--page-size');
define('CMD_PAGE_WIDTH', '--page-width');
define('CMD_PRINT_MEDIA_TYPE', '--print-media-type');
define('CMD_REPLACE', '--replace');
define('CMD_USER_STYLE_SHEET', '--user-style-sheet');
define('CMD_FOOTER_CENTER', '--footer-center');
define('CMD_FOOTER_FONT_NAME', '--footer-font-name');
define('CMD_FOOTER_FONT_SIZE', '--footer-font-size');
define('CMD_FOOTER_HTML', '--footer-html');
define('CMD_FOOTER_LINE', '--footer-line');
define('CMD_FOOTER_RIGHT', '--footer-right');
define('CMD_FOOTER_SPACING', '--footer-spacing');
define('CMD_TITLE', '--title');
define('CMD_DEFAULT_HEADER', '--default-header');
define('CMD_HEADER_CENTER', '--header-center');
define('CMD_HEADER_FONT_NAME', '--header-font-name');
define('CMD_HEADER_FONT_SIZE', '--header-font-size');
define('CMD_HEADER_HTML', '--header-html');
define('CMD_HEADER_LEFT', '--header-left');
define('CMD_HEADER_LINE', '--header-line');
define('CMD_HEADER_RIGHT', '--header-right');
define('CMD_HEADER_SPACING', '--header-spacing');
define('CMD_NO_OUTLINE', '--no-outline');
define('CMD_FOOTER_LEFT', '--footer-left');
define('CMD_LOAD_ERROR_HANDLING', '--load-error-handling');

//http://wkhtmltopdfwithqtubuntuoneric.blogspot.sg/

class WkhtmlToPdf {

  private function is_valid_options($options) {
    if (!empty($options) && is_array($options)) {
      return TRUE;
    }
    return FALSE;
  }

  private function is_key_available($options, $key_to_verify) {
    if ($this->is_valid_options($options)) {
      if (isset($options[$key_to_verify])) {
        if ($options[$key_to_verify] == FALSE) {
          return FALSE;
        }
        return TRUE;
      }
    }
    return FALSE;
  }

  protected $_book = '--book';
  protected $_collate = '--collate';

  /**
   * Function to print pdf in book format
   * @param type $options - array with keys book, collate and with boolean values
   *
   */
  public function enable_book($options) {
    if ($this->is_key_available($options, 'book')) {
      $this->_book = $options['book'];
      $this->_pdf_options .= ' ' . CMD_BOOK;
    }

    if ($this->is_key_available($options, 'collate')) {
      $this->_collate = $options['collate'];
      $this->_pdf_options .= ' ' . CMD_COLLATE;
    }
  }

  protected $_disable_external_links = '--disable-external-links';
  protected $_disable_internal_links = '--disable-internal-links';
  /**
   * Function to disable internal or external links within the PDF.
   * Defaults to FALSE
   * @param type $options - array() with keys external, internal and
   * with boolean values
   */
  public function disable_links($options) {

    if ($this->is_key_available($options, 'external')) {
      $this->_disable_external_links = $options['external'];
      $this->_pdf_options .= ' ' . CMD_DISABLE_EXTERNAL_LINKS;
    }

    if ($this->is_key_available($options, 'internal')) {
      $this->_disable_internal_links = $options['internal'];
      $this->_pdf_options .= ' ' . CMD_DISABLE_INTERNAL_LINKS;
    }
  }

  protected $_forms;

  /**
   * Function to print pdf as Form
   * @param type $options - array with key 'forms' and boolean value.
   */
  function print_form($options) {
    if ($this->is_key_available($options, 'forms')) {
      $this->_forms = $options['forms'];
      $this->_pdf_options .= ' ' . CMD_FORMS;
    }
  }

  protected $_margin_top;
  protected $_margin_bottom;
  protected $_margin_left;
  protected $_margin_right;

  /**
   * Function to set margin
   * unit - millimeter
   * @param type $options - array() available keys are margin_top, margin_bottom,margin_left,
   * margin_right.
   */
  function set_margin($options) {
    if ($this->is_key_available($options, 'margin_top')) {
      $this->_margin_top = $options['margin_top'];
      $this->_pdf_options .= ' ' . CMD_MARGIN_TOP . ' ' . $this->_margin_top;
    }

    if ($this->is_key_available($options, 'margin_bottom')) {
      $this->_margin_bottom = $options['margin_bottom'];
      $this->_pdf_options .= ' ' . CMD_MARGIN_BOTTOM . ' ' . $this->_margin_bottom;
    }

    if ($this->is_key_available($options, 'margin_left')) {
      $this->_margin_left = $options['margin_left'];
      $this->_pdf_options .= ' ' . CMD_MARGIN_LEFT . ' ' . $this->_margin_left;
    }

    if ($this->is_key_available($options, 'margin_right')) {
      $this->_margin_right = $options['margin_right'];
      $this->_pdf_options .= ' ' . CMD_MARGIN_RIGHT . ' ' . $this->_margin_right;
    }
  }

  protected $_replace;

  /**
   * Function to replace a token with some other value. [repeatable]
   * @param type $options - array() where key will act as token and value will act as replacement value.
   */
  function replace_text($options) {
    if ($this->is_valid_options($options)) {
      $this->_replace = $options;
      foreach ($options as $key => $value) {
        $this->_pdf_options .= ' ' . CMD_REPLACE . ' "' . $key . '" "' . $value . '"';
      }
    }
  }

  protected $_print_media_type;

  function set_print_media() {
    //@todo: find available options
  }

  protected $_style_sheet_path;
  protected $_no_background;
  protected $_grayscale;
  protected $_low_quality;
  protected $_no_outline;

  /**
   * Function to set style options
   * @param type $options - array() available keys are
   * 1.style_sheet => style sheet url
   * 2.no_background => boolean
   * 3.grayscale => boolean
   * 4.low_quality => boolean
   * 5.no_outline => boolean
   */
  function set_style($options) {
    if ($this->is_key_available($options, 'style_sheet')) {
      $this->_style_sheet_path = $options['style_sheet'];
      $this->_pdf_options .= ' ' . CMD_USER_STYLE_SHEET . ' ' . $this->_style_sheet_path;
    }

    if ($this->is_key_available($options, 'no_background')) {
      $this->_no_background = $options['no_background'];
      $this->_pdf_options .= ' ' . CMD_NO_BACKGROUND;
    }

    if ($this->is_key_available($options, 'grayscale')) {
      $this->_grayscale = $options['grayscale'];
      $this->_pdf_options .= ' ' . CMD_GRAYSCALE;
    }

    if ($this->is_key_available($options, 'low_quality')) {
      $this->_low_quality = $options['low_quality'];
      $this->_pdf_options .= ' ' . CMD_LOWQUALITY;
    }

    if ($this->is_key_available($options, 'no_outline')) {
      $this->_no_outline = $options['no_outline'];
      $this->_pdf_options .= ' ' . CMD_NO_OUTLINE;
    }
  }

  protected $_title;
  protected $_default_header;
  protected $_header_center;
  protected $_header_font_name;
  protected $_header_font_size;
  protected $_header_html;
  protected $_header_left;
  protected $_header_line;
  protected $_header_right;
  protected $_header_spacing;

  /**
   * Function to header details
   * @param type $options - array() available keys are
   * 1. title => string {The title of the generated pdf file (The title of the first document is used if not specified)}
   * 2. default_header => boolean {Add a default header, with the name of the page to the left, and the page number to the right}
   * 3. header_center => string {Centered header text.}
   * 4. header_font_name => string
   * 5. header_font_size => a valid number
   * 6. header_html => url {Adds a html header.}
   * 7. header_left => string {Left aligned header text.}
   * 8. header_right => string {Right aligned header text.}
   * 9. header_spacing => int {Spacing between header and content in mm (default 0)}
   * 10. header_line => boolean {Display line below the header.}
   */
  function set_header_details($options) {
    if ($this->is_key_available($options, 'title')) {
      $this->_title = $options['title'];
      $this->_pdf_options .= ' ' . CMD_TITLE . ' "' . $this->_title . '"';
    }

    if ($this->is_key_available($options, 'default_header')) {
      $this->_default_header = $options['default_header'];
      $this->_pdf_options .= ' ' . CMD_DEFAULT_HEADER . ' "' . $this->_default_header . '"';
    }

    if ($this->is_key_available($options, 'header_center')) {
      $this->_header_center = $options['header_center'];
      $this->_pdf_options .= ' ' . CMD_HEADER_CENTER . ' "' . $this->_header_center . '"';
    }
    if ($this->is_key_available($options, 'header_font_name')) {
      $this->_header_font_name = $options['header_font_name'];
      $this->_pdf_options .= ' ' . CMD_HEADER_FONT_NAME . ' "' . $this->_header_font_name . '"';
    }
    if ($this->is_key_available($options, 'header_font_size')) {
      $this->_header_font_size = $options['header_font_size'];
      $this->_pdf_options .= ' ' . CMD_HEADER_FONT_SIZE . ' ' . $this->_header_font_size;
    }
    if ($this->is_key_available($options, 'header_html')) {
      $this->_header_html = $options['header_html'];
      $this->_pdf_options .= ' ' . CMD_HEADER_HTML . ' "' . $this->_header_html . '"';
    }
    if ($this->is_key_available($options, 'header_left')) {
      $this->_header_left = $options['header_left'];
      $this->_pdf_options .= ' ' . CMD_HEADER_LEFT . ' "' . $this->_header_left . '"';
    }
    if ($this->is_key_available($options, 'header_spacing')) {
      $this->_header_spacing = $options['header_spacing'];
      $this->_pdf_options .= ' ' . CMD_HEADER_SPACING . ' ' . $this->_header_spacing;
    }
    if ($this->is_key_available($options, 'header_line')) {
      $this->_header_line = $options['header_line'];
      $this->_pdf_options .= ' ' . CMD_HEADER_LINE;
    }
  }

  protected $_footer_center;
  protected $_footer_font_name;
  protected $_footer_font_size;
  protected $_footer_html;
  protected $_footer_left;
  protected $_footer_line;
  protected $_footer_right;
  protected $_footer_spacing;
  protected $_footer_set_defaults = TRUE;

  /**
   *
   * @param type $options - array() available keys are
   * 1. footer_center => string {Centered footer text.} Defaults to page number.
   * 2. footer_font_name => string
   * 3. footer_font_size => int
   * 4. footer_html => url {Adds a html footer.}
   * 5. footer_left => string {Left aligned footer text.}
   * 6. footer_line => boolean {Display line above the footer.}
   * 7. footer_right => string {Right aligned footer text.}
   * 8. footer_spacing => int {Spacing between footer and content in mm (default 0).}
   */
  function set_footer_details($options) {
    if ($this->is_key_available($options, 'footer_center')) {
      $this->_footer_center = $options['footer_center'];
      $this->_pdf_options .= ' ' . CMD_FOOTER_CENTER . ' "' . $this->_footer_center . '"';
    }
    if ($this->is_key_available($options, 'footer_font_name')) {
      $this->_footer_font_name = $options['footer_font_name'];
      $this->_pdf_options .= ' ' . CMD_FOOTER_FONT_NAME . ' "' . $this->_footer_font_name . '"';
    }
    if ($this->is_key_available($options, 'footer_font_size')) {
      $this->_footer_font_size = $options['footer_font_size'];
      $this->_pdf_options .= ' ' . CMD_FOOTER_FONT_SIZE . ' ' . $this->_footer_font_size;
    }
    if ($this->is_key_available($options, 'footer_html')) {
      $this->_footer_html = $options['footer_html'];
      $this->_pdf_options .= ' ' . CMD_FOOTER_HTML . ' "' . $this->_footer_html . '"';
    }
    if ($this->is_key_available($options, 'footer_left')) {
      $this->_footer_left = $options['footer_left'];
      $this->_pdf_options .= ' ' . CMD_FOOTER_LEFT . ' "' . $this->_footer_left . '"';
    }
    if ($this->is_key_available($options, 'footer_line')) {
      $this->_footer_line = $options['footer_line'];
      $this->_pdf_options .= ' ' . CMD_FOOTER_LINE . ' ' . $this->_footer_line;
    }
    if ($this->is_key_available($options, 'footer_right')) {
      $this->_footer_right = $options['footer_line'];
      $this->_pdf_options .= ' ' . CMD_FOOTER_RIGHT . ' "' . $this->_footer_right . '"';
    }
    if ($this->is_key_available($options, 'footer_spacing')) {
      $this->_footer_spacing = $options['footer_spacing'];
      $this->_pdf_options .= ' ' . CMD_FOOTER_SPACING . ' ' . $this->_footer_spacing;
    }
    if($this->is_key_available($options, 'footer_set_defaults')) {
      $this->_footer_set_defaults = $options['footer_set_defaults'];
    } else {
      $this->_footer_set_defaults = TRUE;
    }
  }

  protected $_load_error_handling;
  protected $_pdf_options;

  /**
   * Desired paper size ('letter', 'legal', 'A4', etc.)
   *
   * @var string
   */
  protected $_paper_size;

  /**
   * Paper orientation ('portrait' or 'landscape')
   *
   * @var string
   */
  protected $_paper_orientation;

  /**
   * DPI for images.
   *
   * @var int
   */
  protected $_dpi;

  /**
   * The html content to be converted.
   *
   * @var string
   */
  protected $_html;

  /**
   * The pdf results.
   *
   * @var string
   */
  protected $_pdf;

  /**
   * Anything written to stderr.
   *
   * @var string
   */
  protected $_errors;

/**
 * Token for page number {can be used only in header and footer region.}
 *
 * @var string
 */
  protected $page_number = '[page]';

  /**
   *Token for last page number {can be used only in header and footer region.}
   *
   * @var string
   */
  protected $total_page_count = '[topage]';

  /**
   * Replaced by the number of the first page to be printed
   *
   * @var string
   */
  protected $first_page_number = '[frompage]';

  /**
   * Replaced by the current date in system local format
   *
   * @var type
   */
  protected $current_date = '[date]';

  /**
   * Replaced by the title of the of the current page object
   *
   * @var type
   */
  protected $page_title = '[title]';

  /**
   * Replaced by the title of the output document
   *
   * @var type
   */
  protected $doc_title = '[doctitle]';

  /**
   * Class constructor
   */
  function __construct() {
    $this->_paper_size =  CORE_PDF_PAPER_SIZE_DEFAULT;
    $this->_paper_orientation =  CORE_PDF_PAGE_ORIENTATION_DEFAULT;
    $this->_dpi = CORE_PDF_DPI;
    $this->_load_error_handling = 'ignore';
    $this->_pdf_options .= ' ' . CMD_LOAD_ERROR_HANDLING . ' ' . $this->_load_error_handling;
    if($this->_footer_set_defaults === TRUE)  {
      $this->_footer_center = $this->page_number;
      $this->_footer_right = \Drupal::service('date.formatter')->format(time(), 'medium');
      $this->_footer_font_size = 6;
      $this->_pdf_options .= " --footer-center $this->_footer_center --footer-right \"$this->_footer_right\" ".CMD_FOOTER_FONT_SIZE. " " . $this->_footer_font_size;
    }
  }

  /**
   * Sets the paper size & orientation
   *
   * @param string $size 'letter', 'legal', 'A4', etc.
   * @param string $orientation 'portrait' or 'landscape'
   */
  function set_paper($size, $orientation = "portrait", $dpi=96) {
    $this->_paper_size = $size;
    $this->_paper_orientation = $orientation;
    $this->_dpi = $dpi;
  }

  /**
   * Loads the given HTML.
   *
   * @param String $str HTML content
   * @param String $add_default_tags
   *  If set to TRUE adds '<html><head>'.drupal_get_css().'</head><body>'
   *  to the $str.
   *
   */
  function load_html($str, $add_default_tags=TRUE, $title) {
    if($add_default_tags) {
      $bare_html_page_renderer = \Drupal::service('bare_html_page_renderer');
      $content = [
        'preview-child' => $render_view
      ];

      $this->_html = $bare_html_page_renderer->renderBarePage($content, $title, 'page');
     } else  {
      $this->_html = $str;
    }
  }

  /**
   * Renders the HTML to PDF
   */
  function render() {

    $tool = $this->__findwkhtmltopdf_path();
    if (empty($tool)) {
      return;
    }
    $print_pdf_pdf_tool = drupal_realpath($tool);
    $print_pdf_paper_size = $this->_paper_size;
    $print_pdf_page_orientation = $this->_paper_orientation;
    $print_pdf_wkhtmltopdf_options = $this->_pdf_options . ' ';

    $dpi = $this->_dpi;


    $descriptor = array(0 => array('pipe', 'r'), 1 => array('pipe', 'w'), 2 => array('pipe', 'w'));
    $cmd = $print_pdf_pdf_tool . " --page-size $print_pdf_paper_size --orientation $print_pdf_page_orientation --dpi $dpi $print_pdf_wkhtmltopdf_options - -";
    \Drupal::logger('core')->notice(t('PDF command generated : @cmd', ['@cmd' => $cmd]));
    $process = proc_open($cmd, $descriptor, $pipes, NULL, NULL);

    if (is_resource($process)) {
      fwrite($pipes[0], $this->_html);
      fclose($pipes[0]);

      $this->_pdf = stream_get_contents($pipes[1]);
      fclose($pipes[1]);

      stream_set_blocking($pipes[2], 0);
      $this->_errors = stream_get_contents($pipes[2]);
      if (!empty($this->_errors)) {
        \Drupal::logger('print_pdf')->notice(t('wkhtmltopdf : @array_el', ['@array_el' =>  print_r($this->_errors, TRUE)]));

      }
      fclose($pipes[2]);

      $retval = proc_terminate($process);
    }
  }

  function __findwkhtmltopdf_path() {
    $pattern = '!^(wkhtmltopdf.*)$!';
    $tool_details = file_scan_directory(drupal_get_path('module', 'core') . '/libraries', $pattern);
    reset($tool_details);
    $tool_path = key($tool_details);

    if (!empty($tool_path)) {
      \Drupal::logger('core')->notice(t('pdf tool path @array_el', ['@array_el' =>  print_r($tool_path, TRUE)]));
      return $tool_path;
    }
  }

  /**
   * Streams the PDF to the client
   *
   * The file will open a download dialog by default.  The options
   * parameter controls the output headers.  Accepted headers are:
   *
   * 'Accept-Ranges' => 1 or 0 - if this is not set to 1, then this
   *    header is not included, off by default this header seems to
   *    have caused some problems despite tha fact that it is supposed
   *    to solve them, so I am leaving it off by default.
   *
   * 'Attachment' => 1 or 0 - if 1, force the browser to open a
   *    download dialog, on (1) by default
   *
   * @param string $filename the name of the streamed file
   * @param array  $options header options (see above)
   */
  function stream($filename, $options = null) {
    if (!is_null($this->_pdf)) {
      if (headers_sent()) {
        die("Unable to stream pdf: headers already sent");
      }

      $response = new Response();
      $response->headers->set('Cache-Control', 'private');
      $response->headers->set('Content-Type', 'application/pdf');

      if (!isset($options["Attachment"])) {
        $options["Attachment"] = true;
      }
      $attachment = $options["Attachment"] ? "attachment" : "inline";
      $response->headers->set('Content-Disposition', "Content-Disposition: $attachment; filename=\"$filename\"");

      if (isset($options['Accept-Ranges']) && $options['Accept-Ranges'] == 1) {
        $response->headers->set('Accept-Ranges', strlen($this->_pdf));

      }

      echo $this->_pdf;
      flush();
    }
    exit();
  }

}
