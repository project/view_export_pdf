<?php

namespace Drupal\fds_nroll_export_view_pdf\Plugin\views\area;

use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;


/**
 * Views area handler to display some configurable result summary.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("exportpdf")
 */
class ExportPdf extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['export_file'] = [
      'default' => '',
    ];

    return $options;
  }

  //return $options;
  /**
   * {@inheritdoc}
   */
   public function buildOptionsForm(&$form, FormStateInterface $form_state) {
     parent::buildOptionsForm($form, $form_state);
     $form['export_file'] = [
       '#title' => 'File Name',
       '#type' => 'textfield',
       '#default_value' => $this->options['export_file'],
     ];
   }
   /**
    * {@inheritdoc}
    */
   public function render($empty = FALSE) {
     if (!isset($this->options['export_file']) || $this->view->style_plugin instanceof DefaultSummary) {
       return [];
     }
     $parameters = \Drupal::routeMatch()->getParameters();
     $exposed_args = $this->view->exposed_raw_input;

     $link = 	Url::fromRoute('view_export_pdf.batch', ['view_name' => $this->view->storage->getOriginalId(), 'display' => $this->view->current_display], ['query' => $exposed_args]);
     $link->setAbsolute(true);
     return [
        '#markup' => '<button><a href="' . $link->toString() .'">PDF</a><button>',
      ];
   }
}
