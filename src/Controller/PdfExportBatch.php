<?php

namespace Drupal\view_export_pdf\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

class PdfExportBatch extends ControllerBase {

  public static function content($type) {
    return array(
      '#type' => 'markup',
      '#markup' => $this->t('Batch process finished'),
    );
  }
  public function initBatch($view_name, $display) {

    if(!$view_name) {
      return;
    }

    // $view = _fds_nroll_export_view_pdf_export($view_name, $display);
    // $render = \Drupal::service('renderer')->render($view);
    // $build = ['#markup' => $render];
    // return $build;
    return _view_export_pdf_export_1($view_name, $display);
    $parameters = \Drupal::routeMatch()->getParameters();
    $link = Url::fromRoute("view.$view_name.$display",[], ['query' => [$parameters]]);
    $link->setAbsolute(true);
    $parameters = \Drupal::request()->query->all();
    $batch = array(
        'title' => t('Exporting'),
        'operations' => array(
          array('_fds_nroll_export_view_pdf_export', array($view_name, $display)),
      ),
      'finished' => '_fds_nroll_export_view_pdf_finished',
      );

      //batch_set($batch);
      //return batch_process($link->toString());
    }
  }
